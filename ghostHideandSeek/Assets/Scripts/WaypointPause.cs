﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class WaypointPause : MonoBehaviour {
    public float waypointPause;
    DOTweenPath path;
    Tween myPathTween;

    Vector3[] waypoints;

    private void Awake()
    {
        path = GetComponent<DOTweenPath>();
    }

    // Use this for initialization
    void Start () {

       

        waypoints = path.GetDrawPoints();

        myPathTween = path.GetTween();
        myPathTween.OnWaypointChange(Pause);
	}
	


    public void Pause (int index)
    {
       // Debug.Log("Waypoint has changed");
        StartCoroutine(WaitForTime(waypointPause));
    }

    private IEnumerator WaitForTime(float v)
    {
        myPathTween.Pause();
        yield return new WaitForSeconds(v);
        myPathTween.Play();
    }
    public void Stop ()
    {
        myPathTween.Pause();
    }
    public void Play()
    {
        myPathTween.Play();
    }
}
