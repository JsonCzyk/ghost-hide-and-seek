﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraFlashLight : MonoBehaviour
{

   public GameObject Ghost;
    public GameObject FlashLightBar;
    public Light FlashLight;
   float MaxLightPower = 100;
    float CurrentLightPower= 100;

    Collider ConeCollider;
    bool canRegen = false;
    bool CanStartTimer = false;
    float timertime;
    float ratio;
    WaypointPause WPause;

    // Use this for initialization
    void Start()
    {
       // LightProjector.SetActive(false);
        ConeCollider = this.gameObject.GetComponent<Collider>();
        ConeCollider.enabled = false;
        FlashLight.enabled = false;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        WPause = Ghost.GetComponent<WaypointPause>();
    }

    // Update is called once per frame
    void Update()
    {
        UseFlashLight();

        //  Debug.Log("isShining " + isShiningLight);
        RegenTimer();
        Regen();
        ratio = (CurrentLightPower / MaxLightPower);
    }

 
    private void OnTriggerStay(Collider other)
    {
        GameObject colGO = other.gameObject;
        if (colGO == Ghost)
        {

            Ghost.GetComponent<GhostLookAt>().LookAtCamera();
            //Found him
        }
    }
    void UseFlashLight()
    {
        
         if(CurrentLightPower <.001)
        {
            CurrentLightPower = .001f;
            ConeCollider.enabled = false;
            FlashLight.enabled = false;
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            //  LightProjector.SetActive(false);

        }
        else if (Input.GetKey(KeyCode.Space) &&CurrentLightPower>.01)
        {
            CanStartTimer = false;
            ConeCollider.enabled = true;
            FlashLight.enabled = true;
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            WPause.Stop();

            // LightProjector.SetActive(true);
            canRegen = false;
            timertime = 1f;
            
            CurrentLightPower -= 0.5f;
          //  Debug.Log("Current Light Power Use: " + CurrentLightPower);
            ModifyFlashLightBar(ratio);

        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            CanStartTimer = true;
            ConeCollider.enabled = false;
            FlashLight.enabled = false;
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            //  LightProjector.SetActive(false);
        }
        
    }
    void RegenTimer()
    {
        if (CanStartTimer == true)
        {
            if (timertime <= 0f)
            {
                timertime = 0;
              //  Debug.Log("timertime is 0");
                CanStartTimer = false;
                canRegen = true;
            }
            else if (timertime > 0)
            {
                timertime -= Time.deltaTime;
            }            
        }
    }

    void Regen ()
    {
        
        if (canRegen == true)
        {
          
            if (CurrentLightPower <= 100)
            {
                CurrentLightPower += 2f;
             //   Debug.Log("Current Light Power Regen: " + CurrentLightPower);
                ModifyFlashLightBar(ratio);
            }

        }
    }

    void ModifyFlashLightBar(float mod) 
    {
       if(mod <=0)
        {
            mod = .001f;
        }
        FlashLightBar.transform.localScale = new Vector3(mod, FlashLightBar.transform.localScale.y, FlashLightBar.transform.localScale.z);

    }






}
