﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostLookAt : MonoBehaviour {

    public GameObject LookTarget;

      float LookAtSpeed = 1.5f;


    float startingAlpha = 80;
	// Update is called once per frame
	void Update () {
       // LookAtCamera();

    }


   public void LookAtCamera()
    {
        if(transform.position.y >= 5.5f)
        {
            var LookTargetModifier = transform.position - new Vector3(0, 5.5f, 0);
            
            Vector3 LookTargetModed = new Vector3(Camera.main.transform.position.x, LookTargetModifier.y, Camera.main.transform.position.z);

            Vector3 RelativePos = LookTargetModed - transform.position;
            Quaternion TmpRotation = Quaternion.LookRotation(RelativePos);
            transform.rotation = TmpRotation;
        }
        else
        {
            Vector3 RelativePos = Camera.main.transform.position - transform.position;
            Quaternion TmpRotation = Quaternion.LookRotation(RelativePos);
            transform.rotation = TmpRotation;
        }
    }

    void ChangeColorToDark()
    {
        
    }
}
